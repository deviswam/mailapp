Important notes for the reviewer:
=================================
1. Sufficient comments are given in code where major refactoring is done.
2. The new components created are easy to follow. New components are created based on SOLID principles mainly around Single Responsibility Principle and Dependency Injection via Protocols.
3. The design is now loosely coupled and clean.
4. Integration tests are implemented along with unit tests to test the real components working along with each other.
5. Error handling is implemented.
6. Since current implementation is being used by Production code, its public interfaces are untouched however their internal implementations now uses new CSVReader and CSVWriter components. However, CSVReaderWriter class is declared 'deprecated' and in future clients should be using new implementations of CSVReader and CSVWriter instead. Hence in the current code wherever CSVReaderWriter class is being used 'deprecated warning' is coming up.
7. The readLine functionality which was previous reading a single character at a time has been improved. 