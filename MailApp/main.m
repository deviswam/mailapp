//
//  main.m
//  MailApp
//
//  Created by Waheed Malik on 28/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
