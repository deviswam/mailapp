//
//  CSVReader.m
//  MailApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import "CSVReader.h"

@implementation CSVReader {
    ReaderInputStream *_readerInputStream;
}

- (instancetype)init
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"-init is not a valid initializer for the class. Call initWithInputStream:(ReaderInputStream *)readerInputStream instead."
                                 userInfo:nil];
    return nil;
}

- (instancetype)initWithInputStream:(ReaderInputStream *)readerInputStream {
    self = [super init];
    if (self) {
        _readerInputStream = readerInputStream;
    }
    return self;
}

- (void)close {
    [_readerInputStream close];
}

- (void)open:(NSString*)filePath error:(NSError **)error {
    [_readerInputStream open:filePath error:error];
}

- (BOOL)read:(NSMutableArray*)columns withColumnSeperator:(char)columnSeperator lineSeperator:(char)lineSeperator error:(NSError **)error {
    NSError *readError = nil;
    NSString *line = [_readerInputStream readLineWithLineSeperator:lineSeperator error:&readError];
    
    if (readError) {
        *error = readError;
    } else if (line && [line length] > 0) {
        [columns removeAllObjects];
        [columns addObjectsFromArray:[line componentsSeparatedByString:[NSString stringWithFormat:@"%c",columnSeperator]]];
        return YES;
    }
    return NO;
}

-(void)dealloc {
    [self close];
}

@end
