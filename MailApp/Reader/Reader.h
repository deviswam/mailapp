//
//  Reader.h
//  MailApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

@protocol Reader <NSObject>

-(void)open:(NSString*)filePath error:(NSError **)error;
-(void)close;
-(BOOL)read:(NSMutableArray*)columns withColumnSeperator:(char)columnSeperator lineSeperator:(char)lineSeperator error:(NSError **)error;

@end
