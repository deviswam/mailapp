//
//  NSInputStream+Utility.m
//  TestApp
//
//  Created by Waheed Malik on 22/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import "ReaderInputStream.h"

#define BYTES_TO_READ 1024

@interface ReaderInputStream ()
// Buffer to hold any unprocessed string data after each loop.
@property (strong, nonatomic) NSString *stringBuffer;
@end

@implementation ReaderInputStream

- (void)open:(NSString *)filePath error:(NSError **)error {
    // Ensure we have a file to read.
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileNoSuchFileError userInfo:nil];
            return;
    }
    
    // Create
    _inputStream = [NSInputStream inputStreamWithFileAtPath:filePath];
    
    // Input stream is created.
    if (!self.inputStream) {
        *error = [self.inputStream streamError];
        return;
    }
    
    // Open the stream.
    [self.inputStream open];
    
    // Check for any errors at this point.
    if ([self.inputStream streamStatus] == NSStreamStatusError) {
        *error = [self.inputStream streamError];
        return;
    }
}

- (void)close {
    [self.inputStream close];
    _inputStream = nil;
}

- (NSString*)readLineWithLineSeperator:(char)lineSeperator error:(NSError **)error {
    
    uint8_t buf[BYTES_TO_READ];
    NSInteger len = 0;
    NSString *lineToReturn = nil;
    
    len = [self.inputStream read:buf maxLength:BYTES_TO_READ];
    
    // Incase stream encountered error
    if ([self.inputStream streamStatus] == NSStreamStatusError) {
        *error = [self.inputStream streamError];
        return nil;
    }
    
    if (len) { // Stream has data
        if (self.stringBuffer) {
            // Some data left from the last time this method was called so append the new data.
            self.stringBuffer = [self.stringBuffer stringByAppendingString:[[NSString alloc] initWithBytes:buf length:len encoding:NSUTF8StringEncoding]];
        } else {
            // No data left over from last time.
            self.stringBuffer = [[NSString alloc] initWithBytes:buf length:len encoding:NSUTF8StringEncoding];
        }
    }
    
    // Split on newlines.
    NSString *lSeperator = [NSString stringWithFormat:@"%c", lineSeperator];
    NSMutableArray *lines = [[self.stringBuffer componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:lSeperator]] mutableCopy];
    if (lines.count > 0) {
        lineToReturn = lines[0];
        [lines removeObjectAtIndex:0];
        self.stringBuffer = [lines componentsJoinedByString:lSeperator];
    }
    
    return lineToReturn;
}

- (void)dealloc
{
    [self close];
}

@end

