//
//  CSVReader.h
//  MailApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reader.h"
#import "ReaderInputStream.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSVReader : NSObject <Reader>
- (instancetype)initWithInputStream:(ReaderInputStream *)readerInputStream;
@end

NS_ASSUME_NONNULL_END
