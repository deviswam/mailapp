//
//  NSInputStream+Utility.h
//  TestApp
//
//  Created by Waheed Malik on 22/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ReaderInputStream: NSObject

@property (strong, nonatomic, readonly) NSInputStream *inputStream;

- (void)open:(NSString *)filePath error:(NSError **)error;
- (void)close;
- (NSString*)readLineWithLineSeperator:(char)lineSeperator error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
