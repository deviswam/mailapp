//
//  AppDelegate.h
//  MailApp
//
//  Created by Waheed Malik on 28/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

