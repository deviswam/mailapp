/*
 A junior developer was tasked with writing a reusable implementation for a mass mailing application to read and write text files that hold tab separated data.
 
 His implementation, although it works and meets the needs of the application, is of very low quality.
 
 Your task:
 
 - Identify and annotate the shortcomings in the current implementation as if you were doing a code review, using comments in the source files.
 
 - Refactor the CSVReaderWriter implementation into clean, idiomatic, elegant, rock-solid & well performing code, without over-engineering.
 
 - Where you make trade offs, comment & explain.
 
 - Assume this code is in production and backwards compatibility must be maintained. Therefore if you decide to change the public interface, please deprecate the existing methods. Feel free to evolve the code in other ways though. You have carte blanche while respecting the above constraints. 
 */

#import "CSVReaderWriter.h"

@implementation CSVReaderWriter {
    NSInputStream* inputStream;
    NSOutputStream* outputStream;
}

- (instancetype)init
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"-init is not a valid initializer for the class. Call initWithReader:(id<Reader>)reader writer:(id<Writer>)writer instead."
                                 userInfo:nil];
    return nil;
}

- (instancetype)initWithReader:(id<Reader>)reader writer:(id<Writer>)writer {
    self = [super init];
    _reader = reader;
    _writer = writer;
    return self;
}

- (void)open:(NSString*)path mode:(FileMode)mode {
    switch (mode) {
        case FileModeRead:
        {
            [_reader open:path error:nil];
            break;
        }
        case FileModeWrite:
        {
            [_writer open:path error:nil];
            break;
        }
        default:
        {
            NSException* ex = [NSException exceptionWithName:@"UnknownFileModeException"
                                                      reason:@"Unknown file mode specified"
                                                    userInfo:nil];
            @throw ex;
            break;
        }
    }
}

- (BOOL)read:(NSMutableString**)column1 column2:(NSMutableString**)column2 {
    NSMutableArray *columns = [NSMutableArray array];
    BOOL isRead = [self read:columns];
    if (isRead && columns.count >= 2) {
        *column1 = columns[0];
        *column2 = columns[1];
        return YES;
    }
    return NO;
}

- (BOOL)read:(NSMutableArray*)columns {
    NSError *error = nil;
    BOOL isRead = [_reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:&error];
    
    if (!error && isRead) {
        return YES;
    }
    return NO;
}

- (void)write:(NSArray*)columns {
    [_writer write:columns withColumnSeperator:'\t' lineSeperator:'\n' error:nil];
}

- (void)close {
    [_reader close];
    [_writer close];
}

- (void)dealloc
{
    [self close];
}

@end
