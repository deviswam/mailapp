//
//  NSOutputStream+Utility.m
//  TestApp
//
//  Created by Waheed Malik on 22/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import "WriterOutputStream.h"

@implementation WriterOutputStream

- (void)open:(NSString *)filePath error:(NSError **)error {
    // Ensure we have a file to write.
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileNoSuchFileError userInfo:nil];
        return;
    }
    
    // Create
    _outputStream = [NSOutputStream outputStreamToFileAtPath:filePath append:NO];
    
    // Output stream is created.
    if (!self.outputStream) {
        *error = [self.outputStream streamError];
        return;
    }
    
    // Open the stream.
    [self.outputStream open];
    
    // Check for any errors at this point.
    if ([self.outputStream streamStatus] == NSStreamStatusError) {
        *error = [self.outputStream streamError];
        return;
    }
}

- (void)close {
    [self.outputStream close];
    _outputStream = nil;
}

- (void)writeLine:(NSString*)line withLineSeperator:(char)lineSeperator error:(NSError **)error {
    NSData* data = [line dataUsingEncoding:NSUTF8StringEncoding];

    const void* bytes = [data bytes];
    [self.outputStream write:bytes maxLength:[data length]];
 
    uint8_t lsPointer = lineSeperator;
    [self.outputStream write: &lsPointer maxLength: 1];
    
    // Incase stream encountered error
    if ([self.outputStream streamStatus] == NSStreamStatusError) {
        *error = [self.outputStream streamError];
    }
}

- (void)dealloc
{
    [self close];
}
@end
