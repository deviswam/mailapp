//
//  CSVWriter.h
//  TestApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Writer.h"
#import "WriterOutputStream.h"

NS_ASSUME_NONNULL_BEGIN

@interface CSVWriter : NSObject <Writer>
- (instancetype)initWithOutputStream:(WriterOutputStream *)writerOutputStream;
@end

NS_ASSUME_NONNULL_END
