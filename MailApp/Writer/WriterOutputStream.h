//
//  NSOutputStream+Utility.h
//  TestApp
//
//  Created by Waheed Malik on 22/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WriterOutputStream: NSObject

@property (strong, nonatomic, readonly) NSOutputStream *outputStream;

- (void)open:(NSString *)filePath error:(NSError **)error;
- (void)close;
- (void)writeLine:(NSString*)line withLineSeperator:(char)lineSeperator error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
