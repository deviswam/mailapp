//
//  CSVWriter.m
//  TestApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import "CSVWriter.h"

@implementation CSVWriter {
    WriterOutputStream *_writerOutputStream;
}

- (instancetype)init
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"-init is not a valid initializer for the class. Call initWithOutputStream:(WriterOutputStream *)writerOutputStream instead."
                                 userInfo:nil];
    return nil;
}

- (instancetype)initWithOutputStream:(WriterOutputStream *)writerOutputStream {
    self = [super init];
    if (self) {
        _writerOutputStream = writerOutputStream;
    }
    return self;
}

- (void)close {
    [_writerOutputStream close];
}

- (void)open:(NSString*)filePath error:(NSError **)error {
    [_writerOutputStream open:filePath error:error];
}

-(void)write:(NSArray*)columns withColumnSeperator:(char)columnSeperator lineSeperator:(char)lineSeperator error:(NSError **)error {
    NSMutableString* outPut = [@"" mutableCopy];
    
    for (int i = 0; i < [columns count]; i++) {
        [outPut appendString: columns[i]];
        if (([columns count] - 1) != i) {
            [outPut appendString: [NSString stringWithFormat:@"%c", columnSeperator]];
        }
    }
    [_writerOutputStream writeLine:outPut withLineSeperator:lineSeperator error:error];
}

- (void)dealloc
{
    [self close];
}

@end
