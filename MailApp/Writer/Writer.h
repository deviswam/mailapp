//
//  Writer.h
//  TestApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

@protocol Writer <NSObject>

-(void)open:(NSString*)filePath error:(NSError **)error;
-(void)close;
-(void)write:(NSArray*)columns withColumnSeperator:(char)columnSeperator lineSeperator:(char)lineSeperator error:(NSError **)error;

@end
