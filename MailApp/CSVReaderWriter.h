//
//  CSVReaderWriter.h
//  MailApp
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reader.h"
#import "Writer.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, FileMode) {
    FileModeRead = 1,
    FileModeWrite = 2
};

@interface CSVReaderWriter : NSObject

- (instancetype)initWithReader:(id<Reader>)reader writer:(id<Writer>)writer;

@property(nonatomic, readonly) id<Reader> reader;
@property(nonatomic, readonly) id<Writer> writer;

- (void)open:(NSString*)path mode:(FileMode)mode;
- (BOOL)read:(NSMutableString**)column1 column2:(NSMutableString**)column2;
- (BOOL)read:(NSMutableArray*)columns;
- (void)write:(NSArray*)columns;
- (void)close;

@end

NS_ASSUME_NONNULL_END
