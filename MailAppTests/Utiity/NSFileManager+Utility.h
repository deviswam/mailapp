//
//  NSFileManager+Utility.h
//  TestAppTests
//
//  Created by Waheed Malik on 28/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSFileManager (Utility)
-(NSString *)documentsDirectoryWithFileName:(NSString *)fileName;
@end

NS_ASSUME_NONNULL_END
