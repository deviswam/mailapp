//
//  NSFileManager+Utility.m
//  TestAppTests
//
//  Created by Waheed Malik on 28/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import "NSFileManager+Utility.h"

@implementation NSFileManager (Utility)

-(NSString *)documentsDirectoryWithFileName:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}

@end
