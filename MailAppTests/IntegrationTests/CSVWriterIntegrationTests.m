//
//  CSVWriterIntegrationTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 24/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSVWriter.h"
#import "WriterOutputStream.h"
#import "NSFileManager+Utility.h"

@interface CSVWriterIntegrationTests : XCTestCase

@end

@implementation CSVWriterIntegrationTests {
    CSVWriter *writer;
    NSString* filePath;
}

- (void)setUp {
    filePath = [[NSFileManager defaultManager] documentsDirectoryWithFileName:@"Contacts-To-Write.csv"];
    [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
    WriterOutputStream *writerOutputStream = [[WriterOutputStream alloc] init];
    writer = [[CSVWriter alloc] initWithOutputStream:writerOutputStream];
}

- (void)tearDown {
    writer = nil;
    filePath = nil;
    // remove the file.
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
}

- (void) testWrite_shouldWriteARecordWithTwoColumnsToFile {
    // Assign
    NSError *error = nil;
    // Act
    [writer open:filePath error:&error];
    [writer write:@[@"Waheed",@"123 London Road"] withColumnSeperator:'\t' lineSeperator:'\n' error:&error];
    [writer close];

    // Assert
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    NSString *record = [NSString stringWithUTF8String:[data bytes]];

    XCTAssert([@"Waheed\t123 London Road\n" isEqualToString:record]);
}

- (void) testWrite_shouldWriteARecordWithFiveColumnsToFile {
    // Assign
    NSError *error = nil;
    // Act
    [writer open:filePath error:&error];
    [writer write:@[@"Waheed",@"30",@"123 Luton Road",@"Edinburgh",@"SE186FN"] withColumnSeperator:'\t' lineSeperator:'\n' error:&error];
    [writer close];

    // Assert
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    NSString *record = [NSString stringWithUTF8String:[data bytes]];

    XCTAssert([@"Waheed\t30\t123 Luton Road\tEdinburgh\tSE186FN\n" isEqualToString:record]);
}

@end
