//
//  CSVReaderIntegrationTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 24/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSVReader.h"
#import "ReaderInputStream.h"

@interface CSVReaderIntegrationTests : XCTestCase

@end

@implementation CSVReaderIntegrationTests {
    CSVReader *reader;
    NSString* filePath;
}

- (void)setUp {
    filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"Contacts-To-Read"
                                                     ofType:@"csv"];
    ReaderInputStream *readerInputStream = [[ReaderInputStream alloc] init];
    reader = [[CSVReader alloc] initWithInputStream:readerInputStream];
}

- (void)tearDown {
    reader = nil;
}

- (void) testRead_shouldReadARecordOfTwoColumnsFromFile {
    // Assign
    NSError *error = nil;
    // Act
    [reader open:filePath error:&error];
    NSMutableArray *columns = [NSMutableArray array];
    BOOL isRead = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:&error];
    [reader close];
    
    // Assert
    XCTAssert(isRead);
    XCTAssertNil(error);
    XCTAssertEqualObjects(@"Waheed",columns[0]);
    XCTAssertEqualObjects(@"123 London Road", columns[1]);
}

- (void) testRead_shouldReadTwoRecordsOfMultipleColumns {
    // Assign
    NSMutableArray *columns = [NSMutableArray array];
    NSError *error = nil;
    
    // Act
    [reader open:filePath error:&error];
    // Read first record
    BOOL isRead = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n'error:&error];
    // Assert
    XCTAssert(isRead);
    XCTAssertNil(error);
    XCTAssertEqualObjects(@"Waheed",columns[0]);
    XCTAssertEqualObjects(@"123 London Road", columns[1]);
    
    // Read second record
    BOOL isReadSecond = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:&error];
    // Assert
    XCTAssert(isReadSecond);
    XCTAssertNil(error);
    XCTAssert([@"Waheed" isEqualToString:columns[0]]);
    XCTAssert([@"30" isEqualToString:columns[1]]);
    XCTAssert([@"123 Luton Road" isEqualToString:columns[2]]);
    XCTAssert([@"Edinburgh" isEqualToString:columns[3]]);
    XCTAssert([@"SE186FN" isEqualToString:columns[4]]);
    
    [reader close];
}



@end
