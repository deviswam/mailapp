//
//  CSVReaderWriterIntegrationTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 23/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSVReaderWriter.h"
#import "CSVReader.h"
#import "CSVWriter.h"
#import "ReaderInputStream.h"

@interface CSVReaderWriterIntegrationTests : XCTestCase

@end

@implementation CSVReaderWriterIntegrationTests {
    CSVReaderWriter *readerWriter;
    CSVReader *reader;
    CSVWriter *writer;
    NSString *filePath;
}

- (void)setUp {
    // Injecting all the required dependencies.
    filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"Contacts-To-Read"
                                                                          ofType:@"csv"];
    ReaderInputStream *readerInputStream = [[ReaderInputStream alloc] init];
    reader = [[CSVReader alloc] initWithInputStream:readerInputStream];

    readerWriter = [[CSVReaderWriter alloc] initWithReader:reader writer:writer];
}

- (void)tearDown {
    reader = nil;
    writer = nil;
    readerWriter = nil;
    filePath = nil;
}

- (void) testRead_withTwoColumns_shouldReadColumnsFromFile {
    // Act
    [readerWriter open:filePath mode:FileModeRead];
    NSString *column1 = nil;
    NSString *column2 = nil;
    [readerWriter read:&column1 column2:&column2];
    [readerWriter close];

    // Assert
    XCTAssert([@"Waheed" isEqualToString:column1]);
    XCTAssert([@"123 London Road" isEqualToString:column2]);
}

- (void) testRead_forTwoColumnsForMultipleRecords_shouldReadAppropriateData {
    // Act
    [readerWriter open:filePath mode:FileModeRead];
    
    NSString *column1 = nil;
    NSString *column2 = nil;
    
    // Read first record
    [readerWriter read:&column1 column2:&column2];
    //Assert
    XCTAssert([@"Waheed" isEqualToString:column1]);
    XCTAssert([@"123 London Road" isEqualToString:column2]);
    
    // Read second record
    [readerWriter read:&column1 column2:&column2];
    //Assert
    XCTAssert([@"Waheed" isEqualToString:column1]);
    XCTAssert([@"30" isEqualToString:column2]);
    
    [readerWriter close];
}


@end
