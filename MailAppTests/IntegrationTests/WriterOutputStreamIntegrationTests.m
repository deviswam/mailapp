//
//  WriterOutputStreamIntegrationTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 28/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WriterOutputStream.h"
#import "NSFileManager+Utility.h"

@interface WriterOutputStreamIntegrationTests : XCTestCase

@end

@implementation WriterOutputStreamIntegrationTests {
    NSString* filePath;
    WriterOutputStream *writerOutputStream;
}

- (void)setUp {
    writerOutputStream = [[WriterOutputStream alloc] init];
    
    filePath = [[NSFileManager defaultManager] documentsDirectoryWithFileName:@"Contacts-To-Write.csv"];
    [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
}

- (void)tearDown {
    filePath = nil;
    writerOutputStream = nil;
    // remove the file.
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
}

- (void)testOpen_withMissingFile_shouldRaiseNoSuchFileError {
    NSError *error = nil;
    [writerOutputStream open:@"abc.txt" error:&error];
    XCTAssertEqual(error.code, NSFileNoSuchFileError);
}

- (void)testOpen_withValidFilePath_shouldOpenTheFileStreamWithoutError {
    NSError *error = nil;
    [writerOutputStream open:filePath error:&error];
    XCTAssertNil(error);
    XCTAssertEqual([[writerOutputStream outputStream] streamStatus], NSStreamStatusOpen);
}

- (void)testClose_shouldCloseDownTheFileStream {
    [writerOutputStream close];
    XCTAssertEqual([[writerOutputStream outputStream] streamStatus], NSStreamStatusNotOpen);
}

- (void)testWriteLine_whenCalledOnce_shouldWriteOneRecordToFileWithoutError {
    NSError *error = nil;
    [writerOutputStream open:filePath error:&error];
    [writerOutputStream writeLine:@"Waheed\t123 London Road" withLineSeperator:'\n' error:&error];
    
    // RunLoop is used to read the file so need to give it time to run.
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:1.0]];
    
    [writerOutputStream close];
    
    NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
    NSString *record = [NSString stringWithUTF8String:[data bytes]];
    
    XCTAssertNil(error);
    XCTAssert([@"Waheed\t123 London Road\n" isEqualToString:record]);
}

@end
