//
//  ReaderInputStreamTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 27/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ReaderInputStream.h"

@interface ReaderInputStreamTests : XCTestCase

@end

@implementation ReaderInputStreamTests {
    ReaderInputStream *readerInputStream;
    NSString *filePath;
}

- (void)setUp {
    readerInputStream = [[ReaderInputStream alloc] init];
    filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"Contacts-To-Read"
                                                                ofType:@"csv"];
}

- (void)tearDown {
    readerInputStream = nil;
}

- (void)testOpen_withMissingFile_shouldRaiseNoSuchFileError {
    NSError *error = nil;
    [readerInputStream open:@"abc.txt" error:&error];
    XCTAssertEqual(error.code, NSFileNoSuchFileError);
}

- (void)testOpen_withValidFilePath_shouldOpenTheFileStreamWithoutError {
    NSError *error = nil;
    [readerInputStream open:filePath error:&error];
    XCTAssertNil(error);
    XCTAssertEqual([[readerInputStream inputStream] streamStatus], NSStreamStatusOpen);
}

- (void)testClose_shouldCloseDownTheFileStream {
    [readerInputStream close];
    XCTAssertEqual([[readerInputStream inputStream] streamStatus], NSStreamStatusNotOpen);
}

- (void)testReadLine_whenCalledOnce_shouldReadOneLineFromFileWithoutError {
    NSError *error = nil;
    [readerInputStream open:filePath error:&error];
    NSString *line = [readerInputStream readLineWithLineSeperator:'\n' error:&error];
    [readerInputStream close];
    
    XCTAssertNil(error);
    XCTAssertEqualObjects(line, @"Waheed\t123 London Road");
}

- (void)testReadLine_whenCalledTwice_shouldReadTwoLinesFromFileWithoutError {
    NSError *error = nil;
    [readerInputStream open:filePath error:&error];
    
    // Read first line.
    NSString *line = [readerInputStream readLineWithLineSeperator:'\n' error:&error];
    XCTAssertNil(error);
    XCTAssertEqualObjects(line, @"Waheed\t123 London Road");
    
    line = [readerInputStream readLineWithLineSeperator:'\n' error:&error];
    XCTAssertNil(error);
    XCTAssertEqualObjects(line, @"Waheed\t30\t123 Luton Road\tEdinburgh\tSE186FN");
    
    [readerInputStream close];
}

@end
