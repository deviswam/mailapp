//
//  CSVReaderTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 22/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "Reader.h"
#import "CSVReader.h"
#import "ReaderInputStream.h"

@interface CSVReaderTests : XCTestCase

@end

@implementation CSVReaderTests {
    CSVReader *reader;
    id fakeReaderInputStream;
}

- (void)setUp {
    fakeReaderInputStream = OCMClassMock([ReaderInputStream class]);
    reader = [[CSVReader alloc] initWithInputStream:fakeReaderInputStream];
}

- (void)tearDown {
    reader = nil;
}

- (void)testConformanceToReaderProtocol {
    XCTAssert([reader conformsToProtocol:@protocol(Reader)]);
}

- (void)testOpen_opensUpTheReaderInputStream {
    // Act
    [reader open:@"" error:nil];
    // Assert
    OCMVerify([fakeReaderInputStream open:[OCMArg any] error:[OCMArg anyObjectRef]]);
}

- (void)testClose_closesTheInputStream {
    // Act
    [reader close];
    // Assert
    OCMVerify([fakeReaderInputStream close]);
}

- (void)testRead_withNoDataToRead_returnsFalse {
    // Arrange
    NSMutableArray *columns = [NSMutableArray array];
    // Act
    OCMStub([fakeReaderInputStream readLineWithLineSeperator:'\n' error:[OCMArg anyObjectRef]]).andReturn(@"");
    BOOL isRead = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]];
    // Assert
    XCTAssertFalse(isRead);
    XCTAssert(columns.count == 0);
}

- (void)testRead_withOneColumn_shouldCorrectlyReadFormattedData {
    // Arrange
    NSMutableArray *columns = [NSMutableArray array];
    OCMStub([fakeReaderInputStream readLineWithLineSeperator:'\n' error:[OCMArg anyObjectRef]]).andReturn(@"123LondonRoad");
    // Act
    BOOL isRead = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]];
    // Assert
    XCTAssert(isRead);
    XCTAssert(columns.count == 1);
}

- (void)testRead_withTwoColumns_shouldCorrectlyReadFormattedData {
    // Arrange
    NSMutableArray *columns = [NSMutableArray array];
    OCMStub([fakeReaderInputStream readLineWithLineSeperator:'\n' error:[OCMArg anyObjectRef]]).andReturn(@"Waheed\t123LondonRoad");
    // Act
    BOOL isRead = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]];
    // Assert
    XCTAssert(isRead);
    XCTAssert([@"Waheed" isEqualToString:columns[0]]);
    XCTAssert([@"123LondonRoad" isEqualToString:columns[1]]);
}

- (void)testRead_withFiveColumns_shouldCorrectlyReadFormattedData {
    // Arrange
    NSMutableArray *columns = [NSMutableArray array];
    OCMStub([fakeReaderInputStream readLineWithLineSeperator:'\n' error:[OCMArg anyObjectRef]]).andReturn(@"Waheed\t30\t123LondonRoad\tLondon\tSE13");
    // Act
    BOOL isRead = [reader read:columns withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]];
    // Assert
    XCTAssert(isRead);
    XCTAssert([@"Waheed" isEqualToString:columns[0]]);
    XCTAssert([@"30" isEqualToString:columns[1]]);
    XCTAssert([@"123LondonRoad" isEqualToString:columns[2]]);
    XCTAssert([@"London" isEqualToString:columns[3]]);
    XCTAssert([@"SE13" isEqualToString:columns[4]]);
}

@end
