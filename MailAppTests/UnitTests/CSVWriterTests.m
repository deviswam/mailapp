//
//  CSVWriterTests.m
//  TestAppTests
//
//  Created by Waheed Malik on 22/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "Writer.h"
#import "CSVWriter.h"
#import "WriterOutputStream.h"

@interface CSVWriterTests : XCTestCase

@end

@implementation CSVWriterTests {
    CSVWriter *writer;
    id fakeWriterOutputStream;
}

- (void)setUp {
    fakeWriterOutputStream = OCMClassMock([WriterOutputStream class]);
    writer = [[CSVWriter alloc] initWithOutputStream:fakeWriterOutputStream];
}

- (void)tearDown {
    writer = nil;
}

- (void)testConformanceToWriterProtocol {
    XCTAssert([writer conformsToProtocol:@protocol(Writer)]);
}

- (void)testOpen_opensUpTheOutputStream {
    // Act
    [writer open:@"" error:nil];
    // Assert
    OCMVerify([fakeWriterOutputStream open:[OCMArg any] error:[OCMArg anyObjectRef]]);
}

- (void)testClose_closesTheOutputStream {
    // Act
    [writer close];
    // Assert
    OCMVerify([fakeWriterOutputStream close]);
}

- (void)testWrite_withOneColumn_shouldCorrectlyWriteFormattedData {
    // Act
    [writer write:@[@"Waheed"] withColumnSeperator:'\t' lineSeperator:'\n' error:nil];
    // Assert
    OCMVerify([fakeWriterOutputStream writeLine:@"Waheed" withLineSeperator:'\n' error:nil]);
}

- (void)testWrite_withTwoColumns_shouldCorrectlyWriteFormattedData {
    // Act
    [writer write:@[@"Waheed",@"123 London Road"] withColumnSeperator:'\t' lineSeperator:'\n' error:nil];
    // Assert
    OCMVerify([fakeWriterOutputStream writeLine:@"Waheed\t123 London Road" withLineSeperator:'\n' error:nil]);
}

- (void)testWrite_withFiveColumns_shouldCorrectlyWriteFormattedData {
    // Act
    [writer write:@[@"Waheed",@"30",@"123 London Road",@"London",@"SE13"] withColumnSeperator:'\t' lineSeperator:'\n' error:nil];
    // Assert
    OCMVerify([fakeWriterOutputStream writeLine:@"Waheed\t30\t123 London Road\tLondon\tSE13" withLineSeperator:'\n' error:nil]);
}

@end
