//
//  MailAppTests.m
//  MailAppTests
//
//  Created by Waheed Malik on 21/11/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "Reader.h"
#import "Writer.h"
#import "CSVReaderWriter.h"

@interface CSVReaderWriterTests : XCTestCase

@end

@implementation CSVReaderWriterTests {
    CSVReaderWriter *sut;
    id mockReader;
    id mockWriter;
}

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
    mockReader = OCMProtocolMock(@protocol(Reader));
    mockWriter = OCMProtocolMock(@protocol(Writer));
    sut = [[CSVReaderWriter alloc] initWithReader:mockReader writer:mockWriter];
}

- (void)tearDown {
    sut = nil;
}

#pragma mark - OPEN METHOD TESTS

- (void)testOpen_whenFileModeIsRead_shouldAskReaderToOpenFile {
    // Act
    [sut open:@"somefile.csv" mode:FileModeRead];
    // Assert
    OCMVerify([mockReader open:[OCMArg any] error:[OCMArg anyObjectRef]]);
}

- (void)testOpen_whenFileModeIsWrite_shouldAskWriterToOpenFile {
    // Act
    [sut open:@"somefile.csv" mode:FileModeWrite];
    // Assert
    OCMVerify([mockWriter open:[OCMArg any] error:[OCMArg anyObjectRef]]);
}

- (void)testOpen_whenUnknownFileMode_shouldThrowUnknownFileModeException {
    // Arrange
    FileMode unknownFileMode = (FileMode)3;
    // Act and Assert
    XCTAssertThrowsSpecific([sut open:@"somefile.csv" mode:unknownFileMode], NSException, @"should throw an exception");
}

#pragma mark - CLOSE METHOD TESTS

- (void)testClose_shouldAskBothReaderAndWriterToClose {
    // Act
    [sut close];
    // Assert
    OCMVerify([mockReader close]);
    OCMVerify([mockWriter close]);
}

#pragma mark - READ METHOD TESTS

-(void)testRead_withTwoColumns_shouldAskReaderToRead {
    // Act
    NSString *column1 = nil;
    NSString *column2 = nil;
    [sut read:&column1 column2:&column2];

    // Assert
    OCMVerify([mockReader read:[OCMArg any] withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]]);
}

-(void)testRead_withMultipleColumns_shouldAskReaderToRead {
    // Act
    NSMutableArray *columns = nil;
    [sut read:columns];

    // Assert
    OCMVerify([mockReader read:[OCMArg any] withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]]);
}

#pragma mark - WRITE METHOD TESTS

-(void)testWrite_withMultipleColumns_shouldAskWriterToWrite {
    // Act
    NSArray *columns = @[@"Waheed Malik", @"123 London Road"];
    [sut write:columns];

    // Assert
    OCMVerify([mockWriter write:[OCMArg any] withColumnSeperator:'\t' lineSeperator:'\n' error:[OCMArg anyObjectRef]]);
}

@end

